# Ansible deployment for verwerkingenlogging

Deployment repo for https://gitlab.com/commonground/referentie-apis/verwerkingenlogging

Initial setup to install dependencies:

```bash
$ virtualenv env --python=python3
$ pip install ansible~=2.10
$ ansible-galaxy collection install -r requirements.yml
$ ansible-galaxy install -r requirements.yml
```

To deploy the project, update `django_app_docker_sha256` in `vars/defaults.yml` and run the following command:

```bash
$ ansible-playbook verwerkingenlogging.yml -i ansible/inventories/test --ask-vault-pass
```